resource "kubernetes_namespace" "nginx-namespace" {
  metadata {
    name = "nginx-namespace"
  }

  lifecycle {
    ignore_changes = [
      metadata[0].annotations,
      metadata[0].labels,
    ]
  }
}
