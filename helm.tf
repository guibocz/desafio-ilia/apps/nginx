resource "helm_release" "nginx-helm" {
  name      = "nginx-helm"
  chart     = "chart/"
  namespace = kubernetes_namespace.nginx-namespace.metadata.0.name

  depends_on = [
    kubernetes_namespace.nginx-namespace,
  ]
}
