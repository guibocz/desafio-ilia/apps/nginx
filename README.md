# NGINX

## Purpose

Project to deploy a Nginx server using Helm and a local chart.

## Values

You may change the values on the file `chart/values.yaml`

Main values you may change:

| Name                      | Value                                            |
|---------------------------|--------------------------------------------------|
| replicaCount              | quantity of replicas                             |
| image.repository          | image to be used on the container                |
| image.tag                 | tag of the selectec image                        |
| image.pullPolicy          | policy to pull image (IfNotPresent|Always|Never) |
| service.type              | type of the service (LoadBalancer|ClusterIP)     |
| service.port              | port of the service                              |
| resources.limits.cpu      | cpu limit for the container                      |
| resources.limits.memory   | memory limit for the container                   |
| resources.requests.cpu    | cpu request for the container                    |
| resources.requests.memory | memory request for the container                 |

## Dependencies

The CI of this project depends on the following environment variables that should be defined in GitLab:

| Name                    | Value                              |
|-------------------------|------------------------------------|
| AWS_ACCESS_KEY_ID       | access key to aws account          |
| AWS_SECRET_ACCESS_KEY   | secret key to aws account          |
| S3_BUCKET               | name of the bucket on aws          |
| S3_REGION               | region of the bucket on aws        |